NanoAlbum
=========

a simple and small PHP photo album /gallery  
Goals:
* KISS - keep it small and simple
* no database required
* zero configuration / little configuration
* small footprint: basic funtionality in just one single file
* no wasting of display area, use whole screen
* design for modern browsers , html5, css3
* design for mobile devices
* save bandwidth, using client cache where possible
* provide original photos for download and viewing
* Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
* installation: https://github.com/oliworx/NanoAlbum/wiki/install
* demo: http://kurmis.com
